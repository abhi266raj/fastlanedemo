//
//  AppDelegate.h
//  FastalneDemo
//
//  Created by Abhiraj Kumar on 5/11/17.
//  Copyright © 2017 Indivisual. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

